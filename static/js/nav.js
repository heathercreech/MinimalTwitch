document.addEventListener('DOMContentLoaded', function(){
    $('#nav-handle').mouseover(function(){
        $('#nav-handle').addClass('active-handle');
    });

    $('#nav-handle').mouseout(function(){
        $('#nav-handle').removeClass('active-handle');
    });

    $('#nav-handle').click(function(){
       $('#nav').toggle();
       $('#nav-handle').toggleClass('active-handle');
    });

    $('#nav').hide();
});
