$(document).ready(function(){
    $('.twitch-connect').click(function(){
        Twitch.login({
            response_type: 'code',
            redirect_uri: 'http://twitch-dev.heathercreech.me/oauth_callback',
            scope: ['user_read', 'channel_read']
        });
    });
});
