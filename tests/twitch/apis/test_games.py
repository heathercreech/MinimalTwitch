from twitch.apis.games import *
from .util import compare_structures

top_games_struct = [
    'channels', 
    {'game': ['_id', {'box': ['large', 'medium', 'small']},
        'giantbomb_id', {'logo': ['large', 'medium', 'small']}, 'name'
    ]}, 
    'viewers']


class TestGames:
    
    @classmethod
    def setup_class(cls):
        pass

    @classmethod
    def teardown_class(cls):
        pass

    def test_top_games(self):
        assert compare_structures(
                top_games_struct,
                top_games()['top'][0]
            )
