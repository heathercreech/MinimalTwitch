#recusively defines a calls structure
def handle_dict(call_res_dict):
    built_list = []
    for key in call_res_dict:
        value = call_res_dict[key]
        if isinstance(value, dict):
            built_list.append({key: handle_dict(value)})
        else:
            built_list.append(key)
    return built_list

def get_structure_from_results(call_results):
    call_struct = []
    for key in call_results:
        if isinstance(call_results[key], dict):
            built_list = handle_dict(call_results[key])
            call_struct.append({key: built_list})
        else:
            call_struct.append(key)
    return call_struct

def recr_cmp(req, source):
    for item in req:
            if isinstance(item, dict):
                req_key = list(item.keys())[0]
                for source_item in source:
                    if isinstance(source_item, dict):
                        if req_key not in source_item:
                            return False
                        if not recr_cmp(item, source_item):
                            return False
            else:
                if item not in source:
                    return False
    return True

def compare_structures(cmp_struct, call_results):
    call_structure = get_structure_from_results(call_results)

    for item in cmp_struct:
        if isinstance(item, dict):
            cmp_key = list(item.keys())[0]
            call_dict = {}
            for call_item in call_structure:
                if isinstance(call_item, dict):
                    if cmp_key in call_item:
                        call_dict = call_item
                        break
            if not recr_cmp(item, call_dict):
                return False
        else:
            print(item)
            if item not in call_results:
                return False
    return True
