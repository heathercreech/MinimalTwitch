from twitch.apis.channels import *
from .util import compare_structures

channel_info_struct = [
        '_id', 'created_at', 'display_name', 'followers', 
        'game', 'language', 'mature', 'name', 'partner', 
        'status', 'updated_at', 'url', 'views'
    ]
channel_videos_struct = [
       '_id', 'animated_preview', 'broadcast_id', 'broadcast_type',
       {'channel': ['display_name', 'name']}, 'created_at', 
       'description', 'game', 'length', 'preview', 'recorded_at', 
       'title', 'url', 'views'
    ]
channel_followers_struct = [
        'created_at', 
        {'user': ['_id', 'created_at', 'name', 'updated_at']}
    ]
access_token_struct = [
        'sig', 'token'
    ]

class TestChannels:

    def test_channel_info(self):
        assert compare_structures(
                channel_info_struct,
                channel_info('rabidnudist')
            )

    def test_channel_videos(self):
        assert compare_structures(
                channel_videos_struct,
                channel_videos('manvsgame')['videos'][0]
            )

    def test_channel_followers(self):
        assert compare_structures(
                channel_followers_struct,
                channel_followers('rabidnudist')['follows'][0]
            )

    def test_access_token(self):
        assert compare_structures(
                access_token_struct,
                access_token('rabidnudist')
            )
