from twitch.apis.streams import *
from .util import compare_structures
from .test_channels import channel_info_struct

list_streams_struct = [
        '_id', {'channel': channel_info_struct}, 'created_at', 
        'game', {'preview': ['large', 'medium', 'small']}
    ]
channel_stream_struct = [
        list_streams_struct 
    ]
featured_streams_struct = [
        'image', 'priority', 'scheduled', 'sponsored', 
        {'stream': list_streams_struct}, 'text', 'title' 
    ]
summary_struct = [
        'channels', 'viewers'
    ]


class TestStreams:

    @classmethod
    def setup_class(cls):
        pass

    @classmethod
    def teardown_class(cls):
        pass

    def test_list_streams(self):
        assert compare_structures(
                list_streams_struct,
                list_streams(limit=1)['streams'][0]
            )

    def test_channel_stream(self):
        assert compare_structures(
                channel_stream_struct,
                channel_stream('rabidnudist')['stream']
            )

    def test_featured_streams(self):
        assert compare_structures(
                featured_streams_struct,
                featured_streams(limit=1)['featured'][0]
            )

    def test_summary(self):
        assert compare_structures(
                summary_struct,
                summary()
            )
