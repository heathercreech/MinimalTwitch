from twitch.apis.ingests import *
from .util import compare_structures

list_servers_struct = [
        '_id', 'availability', 'name', 'url_template'
    ]


class TestIngests:

    @classmethod
    def setup_class(cls):
        pass

    @classmethod
    def teardown_class(cls):
        pass

    def test_list_servers(self):
        assert compare_structures(
                list_servers_struct,
                list_servers()['ingests'][0]
            )
