from twitch.apis.teams import *
from .util import compare_structures

list_teams_struct = [
        '_id', 'background', 'banner', 'created_at', 'display_name',
        'info', 'name', 'updated_at'
    ]
get_team_struct = [
        '_id', 'background', 'banner', 'created_at', 'display_name',
        'info', 'name', 'updated_at'
    ]


class TestTeams:

    @classmethod
    def setup_class(cls):
        pass

    @classmethod
    def teardown_class(cls):
        pass

    def test_list_teams(self):
        assert compare_structures(
                list_teams_struct,
                list_teams(limit=1)['teams'][0]
            )

    def test_get_team(self):
        assert compare_structures(
                get_team_struct,
                get_team('testteam')
            )
