from twitch.apis.users import *
from .util import compare_structures
from .test_channels import channel_info_struct
from .test_streams import list_streams_struct 

user_info_struct = [
        '_id', 'bio', 'created_at', 'display_name', 'name', 'updated_at'
    ]
followed_channels_struct = [
        {'channel': channel_info_struct}, 'created_at', 'live', {'stream': list_streams_struct}
    ]


class TestUsers:

    @classmethod
    def setup_class(cls):
        pass

    @classmethod
    def teardown_class(cls):
        pass

    def test_user_info(self):
        assert compare_structures(
                user_info_struct,
                user_info('rabidnudist')
            )

    def test_followed_channels(self):
        assert compare_structures(
                followed_channels_struct,
                followed_channels('rabidnudist', limit=1)['follows'][0]
            )
