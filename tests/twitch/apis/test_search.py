from twitch.apis.search import *
from .util import *

#base structure for each relevant item
channel_struct = [
    'display_name', 'followers', 'game', 
    'language', 'logo', 'name', 'partner',
    'status', 'url']
streams_struct = [
    '_id', {'channel': channel_struct}, 
    'created_at', 'delay', 'game', 'is_playlist', 
    'preview', 'video_height', 'viewers']
games_struct = [  
    '_id',
    {'box': [
        'large', 'small', 'medium'
    ]}, 
    'giantbomb_id', 'name']


class TestSearch:
    
    @classmethod
    def setup_class(cls):
        pass
    
    @classmethod
    def teardown_class(cls):
        pass
    
    def test_channels(self):
        assert compare_structures(
                channel_struct,
                channels('rabidnudist')['channels'][0]
            )

    def test_streams(self):
        try:
            res_bool = compare_structures(
                    streams_struct,
                    streams('rabidnudist')['streams'][0]
                )
        except IndexError:
            res_bool = False
                
        assert res_bool

    def test_games(self):
        assert compare_structures(
                games_struct,
                games('League of Legends')['games'][0]
            )


if __name__ == '__main__':
    pass
