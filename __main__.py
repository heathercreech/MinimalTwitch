import json
from os import urandom

from flask import Flask, session, escape, request, redirect, url_for, render_template, make_response
from setproctitle import setproctitle

from following_data import getLiveChannels, getPreviewImages, getStreamObjects
from game_data import getTopGames, getGameStreams
from twitch.apis.users import user
from twitch.apis.streams import channel_stream

app = Flask(__name__)


@app.route('/oauth_callback')
def oauth_callback():
    if 'access_token' in request.args:
        session['access_token'] = escape(request.args.get('access_token'))
        session['username'] = user(session['access_token'])['name']
    return render_template('oauth.html')
   

@app.route('/', methods=['GET', 'POST'])
def login():
    if request.args.get('override_redirect') != 'true':
        if 'username' in session:
            return redirect(url_for('following', username=session['username']))
    if request.method == 'POST' and len(request.form['username']) > 0:
        session['username'] = escape(request.form['username'])
        return redirect(url_for('following', username=session['username']))
    else:
        return render_template('login.html')

@app.route('/following/')
def following_redirect():
    if 'username' in session:
        return redirect(url_for('following', username=session['username']))
    else:
        return redirect(url_for('login'))

@app.route('/following/<username>')
def following(username):
    if 'username' in session and username == session['username']:
        #channels = getStreamObjects(getLiveChannels(username))
        return render_template('following.html', username=escape(username))
    return redirect(url_for('login'))

@app.route('/channel/<channel>')
def channel(channel):
    return render_template('channel.html', channel=channel)

@app.route('/games/')
def gameList():
    return render_template('games.html')

@app.route('/games/<game_name>')
def gameChannels(game_name):
    if game_name:
        #game_channels = getGameStreams(game_name)
        return render_template('game.html', game_name=game_name)
    return redirect(url_for("gameList"))

#in case we use session in the future
def getSecretKey(filepath):
    try:
        with open(filepath) as sk_file:
            return sk_file.readline()
    except FileNotFoundError:
        generated_key = str(urandom(50))
        with open(filepath, 'w') as sk_file:
            sk_file.write(generated_key)
            return generated_key

def registerJinjaFunction(func):
    app.jinja_env.globals[func.__name__] = func
    
if __name__ == '__main__':
    app.secret_key = getSecretKey('secret_key.cfg')
    setproctitle('DMTwitch')
    app.run(debug=True, threaded=True, host='0.0.0.0', port=5050)
