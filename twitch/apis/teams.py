from .api import TwitchAPI


api = TwitchAPI("api", extra_base="kraken/teams")

def list_teams(**params):
    return api.call("", **params)
    
def get_team(team):
    return api.call(team)
