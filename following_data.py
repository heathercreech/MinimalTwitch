from twitch.apis.users import followed_channels

channel_request_limit = 500


#gets a list of all the channels that a user follows
def getFollowedChannels(username):
    return followed_channels(username, limit=500)["follows"]

#gets the live channels that a specific user follows
def getLiveChannels(username):
    following_list = getFollowedChannels(username)
    for channel in following_list:
        if channel["live"]:
            yield channel
            
#
def getStreamObject(channel_obj):
    stream_obj = channel_obj["stream"]
    if stream_obj is not None:
        return channel_obj["stream"]

def getStreamObjects(channels):
    for c in channels:
        yield getStreamObject(c)
        
#returns the large preview image for a live stream
def getPreviewImages(live_channels):
    for channel in live_channels:
        yield getStreamObject(channel)["preview"]["medium"]
